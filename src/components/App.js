import React, {Component} from 'react';
// import {database} from '../firebase';
import _ from 'lodash';  
//help connect to redux store 
import {connect} from 'react-redux';
import '../styles/App.css'
import AddUser from './AddUser'
// import {Link} from 'react-router-dom';
import {getNotes, saveNote, deleteNote, updateNote} from '../actions/notesAction';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      name:'',
      email:'',
      roles:'',
      organisation:'',
      organisation_feature:'',
      country:'',
      id:''

    };
  }  
  //lifecycle

  componentDidMount(){
    //we get this method from redux store's props not above state
    this.props.getNotes();
  }


  handleChange = (e) =>{
    this.setState({
      [e.target.name]:e.target.value, 

    });
  }
  handleSubmit = (e) =>{
    e.preventDefault(); //prevent reload
    const note = {
      name: this.state.name,
      email: this.state.email,
      roles: this.state.roles,
      organisation: this.state.organisation,
      organisation_feature: this.state.organisation_feature,
      country: this.state.country
    };
    //push data to firebase
    this.props.saveNote(note);// passed from store
    this.setState({
      name:'',
      email:'',
      roles:'',
      organisation:'',
      organisation_feature:'',
      country:''
    });
  }
  updateUser = (key)=>{
      const note = this.props.notes[key]
      console.log(note);
      this.setState({
        name:note.name,
        email:note.email,
        roles:note.roles,
        organisation:note.organisation,
        organisation_feature:note.organisation_feature,
        
        country:note.country,
        id:key
      })  
      }
  handleUpdate = (e) => {
    e.preventDefault(); //prevent reload
    const note = {
      name: this.state.name,
      email: this.state.email,
      roles: this.state.roles,
      organisation: this.state.organisation,
      organisation_feature: this.state.organisation_feature,
      country: this.state.country
    };
    this.props.updateNote(this.state.id,note);// passed from store


  }   

  renderNotes = () => {
    //need to get notes from redux store, so now is props instead of state
    return _.map(this.props.notes, (note, key) => {
      //firebase has created a unique key

      return (
              <tr key={key}>
                <th scope="row"></th>
                <td>{note.name}</td>
                <td>{note.email}</td>
                <td>{note.roles}</td>
                <td>{note.organisation}</td>
                <td>{
                    _.map(note.organisation_feature, item=>
                    item+" "
                    )
                  }</td>
                <td>{note.country}</td>
                <td></td>
                <td>
                  <button 
                    className="btn btn-info btn-xs"
                    data-toggle="modal" 
                    data-target="#updateUserModal"  
                    //since we know the key is the unique id
                    onClick={()=>this.updateUser(key)}>
                    Update
                  </button>
                  <button 
                    className="btn btn-danger btn-xs"
                    //since we know the key is the unique id
                    onClick={()=>this.props.deleteNote(key)}>Delete 
                  </button>

                 </td>

              </tr>



      )
    })
  }


  render(){
  return (
    <div className="container-fluid">
        <AddUser />
      <div className="row">
        <div className="col-sm-6 col-sm-offset-3">
            <table className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Organisation</th>
                <th scope="col">Organisation Feature</th>
                <th scope="col">Country</th>
              </tr>
            </thead>
            <tbody>
              {this.renderNotes()}
            </tbody>

          </table>
        </div>
      </div>

      

      {/* Modal for update user */}
      <div className="modal fade" id="updateUserModal"  role="dialog" aria-labelledby="=updateModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="updateModalLabel">User Information</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="modal-body">
            
            <div className="form" >
            <div className="row">
            <div className="col-sm-6 col-sm-offset-3">

            <form onSubmit={this.handleUpdate} id="updateUserForm">
                <div className="form-group">
                <input 
                onChange={this.handleChange}
                value={this.state.name}
                type="text" 
                name="name" 
                className="form-control no-border"
                placeholder="Name..."
                required   
                />
                </div>

                <div className="form-group">
                <input
                onChange={this.handleChange}
                value={this.state.email}
                type="text" 
                name="email" 
                className="form-control no-border"
                placeholder="Email..."
                required  
                />
                </div>

                <div className="form-group">
                <select 
                className="form-control "
                id="addFormControlSelect1"
                value={this.state.roles}
                name="roles"
                onChange={this.handleChange}
                >
                    <option>Roles...</option>
                    <option>Engineer</option>
                    <option>Retail</option>

                </select>
                </div>

                <div className="form-group">
                <input
                onChange={this.handleChange}
                value={this.state.organisation}
                type="text" 
                name="organisation" 
                className="form-control no-border"
                placeholder="Organisation..."
                required  
                />
                </div>

                <div className="form-group">
                <label className="mr-sm-2 col-form-label-sm">Organisation Feature</label>
                <select
                    className="selectpicker"
                    multiple
                    name="organisation_feature"
                    onChange={(e)=>{
   
                    this.setState({
                        organisation_feature:[...e.target.selectedOptions].map(o=>o.value)
                        })
    
                    }}
                    required
                >
                    <option>Trade Vault</option>
                    <option>Inventory</option>
                    <option>Analytics</option>
                </select>
                </div>

                <div className="form-group">
                <select 
                    className="form-control "
                    name="country"
                    onChange={this.handleChange}
                    value={this.state.country}

                >
                    <option>Country</option>
                    <option>Australia</option>
                    <option>New Zealand</option>
                </select>
                </div>

                

                <div className="form-group">
                <button className="btn btn-success col-sm-12" >
                    Save
                </button>
                </div>

            </form>
            </div>
        </div>
        </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary " data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>


        {/* <div className="">
          <nav aria-label="..." className="fixed-bottom page" style={{left:"50%", bottom:"10%"}}>
            <ul className="pagination">
              <li className="page-item disabled">
                <Link className="page-link" tabIndex="-1">Previous</Link>
              </li>
              <li className="page-item">
                <Link className="page-link" to="/1/1">1</Link>
              </li>
              <li className="page-item active">
                <Link className="page-link" to="/1/1">2<span className="sr-only">(current)</span></Link>
              </li>
              <li className="page-item">
              <Link className="page-link" to="/1/1">3<span className="sr-only">(current)</span></Link>
              </li>
              <li className="page-item">
              <Link className="page-link" to="/1/1">Next</Link>
              </li>
            </ul>
          </nav>
        </div> */}
        
    </div>

    

    );


  }
}

//this method comes from react-redux
//we wanna get props from state from redux store, so use this method
function mapStateToProps(state, ownProps){
  return {
    notes: state.notes
  }
}

//connect take 2 arguments: function and actions, then connect to the component
//inform store we're going to use these 2 methods
// the dispatch method is actuall from mapDispatchToProps
//so it's mapping the state to props, then dispatching those props 
export default connect(mapStateToProps, {getNotes, saveNote, deleteNote, updateNote} )(App);
