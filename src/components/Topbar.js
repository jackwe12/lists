import React from 'react';
import '../styles/Topbar.css';


const Topbar = ()=>{
    return (
        <div className="topbar">
            {/* <nav className="navbar fixed-top navbar-light bg-light mb-3">
                <a className="navbar-brand" href="/#">Fixed top</a>
            </nav> */}
            <nav className="border border-primary navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <a className="navbar-brand" href="/#">Page Top Header</a>
            </nav>
        </div>
    )
};


export default Topbar;