import React, {Component} from 'react';
// import {database} from '../firebase';
//help connect to redux store 
import {connect} from 'react-redux';
import {getNotes, saveNote, deleteNote} from '../actions/notesAction4';
import '../styles/AddUser.css'

class AddUser extends Component {
  constructor(props){
    super(props);
    this.state={
      title:'',
      body:'',
      name:'',
      email:'',
      roles:'',
      organisation:'',
      organisation_feature:'',
      country:''
    };

  }  

  componentDidMount(){

    //we get this method from redux store's props not above
    this.props.getNotes();
  }


  handleChange = (e) =>{
    this.setState({
      [e.target.name]:e.target.value, 

    });
  }
  handleSubmit = (e) =>{
    e.preventDefault(); //prevent reload
    const note = {
      name: this.state.name,
      email: this.state.email,
      roles: this.state.roles,
      organisation: this.state.organisation,
      organisation_feature: this.state.organisation_feature,
      country: this.state.country
    };
    // database.push(note);// firebase method, push our data to firebase
    this.props.saveNote(note);// passed from store
    this.setState({
      name:'',
      email:'',
      roles:'',
      organisation:'',
      organisation_feature:'',
      country:''
    });
    // document.getElementById("addUserForm").reset();
  }

  render(){
  return (
      <React.Fragment>
        <div className="addUser">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="/#">Workspace Header</a>
                <button type="button" className="btn btn-success mx-auto" data-toggle="modal" data-target="#addUserModal"
                onClick={()=>{
                  this.setState({
                    name:'',
                    email:'',
                    roles:'',
                    organisation:'',
                    organisation_feature:'',
                    country:''
                  });

                }}
                >
                Add User
                </button>
            </nav>
        </div> 
        
        {/* Modal for add user */}
      <div className="modal fade" id="addUserModal"  role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="addModalLabel">User Information</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="modal-body">
            
            <div className="form" >
            <div className="row">
            <div className="col-sm-6 col-sm-offset-3">

            <form onSubmit={this.handleSubmit} id="addUserForm">
                <div className="form-group">
                <input 
                onChange={this.handleChange}
                value={this.state.name}
                type="text" 
                name="name" 
                className="form-control no-border"
                placeholder="Name..."
                required   
                />
                </div>

                <div className="form-group">
                <input
                onChange={this.handleChange}
                value={this.state.email}
                type="text" 
                name="email" 
                className="form-control no-border"
                placeholder="Email..."
                required  
                />
                </div>

                <div className="form-group">
                <select 
                className="form-control "
                id="updateFormControlSelect1"
                value={this.state.roles}
                name="roles"
                onChange={this.handleChange}
                >
                    <option>Roles...</option>
                    <option>Engineer</option>
                    <option>Retail</option>
                    <option>Owner</option>


                </select>
                </div>

                <div className="form-group">
                <input
                onChange={this.handleChange}
                value={this.state.organisation}
                type="text" 
                name="organisation" 
                className="form-control no-border"
                placeholder="Organisation..."
                required  
                />
                </div>

                <div className="form-group">
                <label className="mr-sm-2 col-form-label-sm">Organisation Feature</label>
                <select
                    className="selectpicker"
                    multiple
                    name="organisation_feature"
                    onChange={(e)=>{
   
                    this.setState({
                        organisation_feature:[...e.target.selectedOptions].map(o=>o.value)
                        })
    
                    }}
                    required
                >
                    <option>Trade Vault</option>
                    <option>Inventory</option>
                    <option>Analytics</option>
                </select>
                </div>

                <div className="form-group">
                <select 
                    className="form-control "
                    name="country"
                    onChange={this.handleChange}
                    value={this.state.country}

                >
                    <option>Country</option>
                    <option>Australia</option>
                    <option>New Zealand</option>
                </select>
                </div>

                

                <div className="form-group">
                <button className="btn btn-success col-sm-12" >
                    Save
                </button>
                </div>

            </form>
            </div>
        </div>
        </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary " data-dismiss="modal">Close</button>
                {/* <button type="button" class="btn btn-primary ">Save changes</button> */}
            </div>
            </div>
        </div>
        </div>

    </React.Fragment>



    );


  }
}

//this method comes from react-redux
//we wanna get props from state from redux store, so use this method
function mapStateToProps(state, ownProps){
  return {
    notes: state.notes
  }
}

//connect take 2 arguments: function and actions, then connect to the component
//inform store we're going to use these 2 methods
// the dispatch method is actuall from mapDispatchToProps
//so it's mapping the state to props, then dispatching those props 
export default connect(mapStateToProps, {getNotes, saveNote, deleteNote} )(AddUser);
