import React from 'react';
import '../styles/Sidebar.css';
// import {Link} from 'react-router-dom';

const Sidebar = ()=>{
    return (
        <div className="wrapper">
            <nav id="sidebar">
                <div className="sidebar-header">
                    <h3>Sidebar</h3>
                </div>

                <ul className="list-unstyled components">
                    <li>
                        {/* <Link className="navbar-brand" to="/">Master record1</Link> */}
                        <a href="/">Master record1</a>
                    </li>
                    <li>
                        {/* <Link className="navbar-brand" to="/2">Master record2</Link> */}
                        <a href="/2">Master record2</a>

                    </li>
                    <li>
                        {/* <Link className="navbar-brand" to="/3">Master record3</Link> */}
                        <a href="/3">Master record3</a>
                    </li>
                    <li>
                        {/* <Link className="navbar-brand" to="/4">Master record4</Link> */}
                        <a href="/4">Master record4</a>

                    </li>
                </ul>
            </nav>

        </div>
    )
};


export default Sidebar;