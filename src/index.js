import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import App2 from './components/App2';
import App3 from './components/App3';
import App4 from './components/App4';

import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootReducer from './reducers/index'
import Topbar from './components/Topbar'
import Sidebar from './components/Sidebar';
import "./styles/index.css"
import {BrowserRouter, Switch, Route} from 'react-router-dom';

// import AddUser from './components/AddUser';
// import notesReducer from './reducers/notesReducer';

//create redux store -> reducers -> actions-actionType | applyMiddleware() when using redux-thunk to make ajax request 

//create store take two arguments
    //rootReducer which is the whole reducer
    //and use composewithdevtools to see redux inside  
    //if no use composewithdevtools then just pass reducer and applymiddle
    //since async action, need thunk


const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

//provide the store to react
ReactDOM.render(
    //now react have the access to global store
    <Provider store={store}>
      <BrowserRouter>
        <Topbar />
        <Sidebar/>
        <div>
            <Switch>
                <Route path="/" component={App} exact={true} />
                <Route path="/2" component={App2} exact={true} />
                <Route path="/3" component={App3} exact={true}/>
                <Route path="/4" component={App4} exact={true}/>
            </Switch>
        </div>
        {/* <App /> */}
      </BrowserRouter>
    </Provider>, document.getElementById('root'));
serviceWorker.unregister();
