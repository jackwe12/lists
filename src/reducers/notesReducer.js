import { GET_NOTES } from '../actionTypes';


//reducer, take action and state
export default function(state={}, action){
    //when distpatch is called and pass action type, will call the case 
    switch(action.type){
        case GET_NOTES:
            return action.payload;
        default:
            return state; 
    }
}