import {GET_NOTES} from '../actionTypes';
import {database2} from '../firebase';


//actionCreators 
export function getNotes(){
    return dispatch => {
        //this is async, so cannot use in redux, instead redux-thunk
        database2.on('value', snapshot => {
            //sent actionType & state to reducer 
            dispatch({
                type: GET_NOTES,
                payload: snapshot.val()
            })
        })  
    }
}

export function saveNote(note) {
    return dispatch => {
        database2.push(note)
    }
}

export function deleteNote(id) {
    //database will find the child of the id then revmove
    return dispatch => database2.child(id).remove()
}

export function updateNote(id,note) {
    //database will find the child of the id then revmove
    return dispatch => database2.child(id).update(note)
}