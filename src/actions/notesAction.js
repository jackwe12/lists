import {GET_NOTES} from '../actionTypes';
import {database} from '../firebase';


//actionCreators 
export function getNotes(){
    return dispatch => {
        //this is async, so cannot use in redux, instead redux-thunk
        database.on('value', snapshot => {
            //sent actionType & state to reducer 
            dispatch({
                type: GET_NOTES,
                payload: snapshot.val()
            })
        })  
    }
}

export function saveNote(note) {
    return dispatch => {
        database.push(note)
    }
}

export function deleteNote(id) {
    //database will find the child of the id then revmove
    return dispatch => database.child(id).remove()
}

export function updateNote(id,note) {
    //database will find the child of the id then revmove
    return dispatch => database.child(id).update(note)
}