import * as firebase from 'firebase'

// Initialise firebase
var firebaseConfig = {
    apiKey: "AIzaSyAezIFbUhjDb7VL1SCXkgIG_0sGLWaozdw",
    authDomain: "lists-dd1a2.firebaseapp.com",
    databaseURL: "https://lists-dd1a2.firebaseio.com",
    projectId: "lists-dd1a2",
    storageBucket: "lists-dd1a2.appspot.com",
    messagingSenderId: "947564506256",
    appId: "1:947564506256:web:47c5caa64a874935583c15",
    measurementId: "G-SM75ER1R6L"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  //database() is method from firebase, and create a reference called notes
  export const database = firebase.database().ref('/notes')
  export const database2 = firebase.database().ref('/notes2')
  export const database3 = firebase.database().ref('/notes3')
  export const database4 = firebase.database().ref('/notes4')